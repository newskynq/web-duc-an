<?php get_header(); ?>
<section class="cnt-banner">
	
</section>
<section class="single-cnt">
	<div class="container">
		<div class="row">
			

			<?php 
			if( have_posts() ) {
				while (have_posts()) {
					the_post();
					$job_id = get_the_ID();
					$gallery = get_field('anbum_anh');
			?>	

				<h1 class="home-title">CỎ NHÂN TẠO SÂN BÓNG</h1>
				<div class="cnt1">
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="cnt-content">
							<div class="image-cnt"><?php the_post_thumbnail(); ?>  </div>
							<?php
								if( $gallery ) {
									foreach($gallery as $image) {
										?>
											<div class="swiper-slide">
												<img src="<?php echo $image['url']; ?>" alt="">		
											</div>
										<?php
									}
								}
							?>	
						</div>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="cnt-right">
							<h3 class="h3-sb"><?php echo the_title(); ?> </h3>
	                        <div class="gia_sb"><span> Giá : </span> <?php echo get_post_meta(get_the_id(),'gia_',true); ?> </div>
	                        <?php if ( get_post_meta(get_the_id(),'danh_gia',true)==1 ) { ?>
	                            <div class="danhgia-sb">
	                                Đánh giá : <i class="fa fa-star" aria-hidden="true"></i></i>
	                            </div>
	                        <?php } ?>
	                        <?php if (get_post_meta(get_the_id(),'danh_gia',true)==2) { ?>
	                            <div class="danhgia-sb">
	                                Đánh giá : <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i>
	                            </div>
	                        <?php } ?>
	                        <?php if (get_post_meta(get_the_id(),'danh_gia',true)==3) { ?>
	                            <div class="danhgia-sb">
	                                Đánh giá : <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i>
	                                            <i class="fa fa-star" aria-hidden="true"></i>
	                            </div>
	                        <?php } ?>
	                        <?php if (get_post_meta(get_the_id(),'danh_gia',true)==4) { ?>
	                            <div class="danhgia-sb">
	                                Đánh giá : <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i>
	                                            <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i>
	                            </div>
	                        <?php } ?>
	                        <?php if (get_post_meta(get_the_id(),'danh_gia',true)==5) { ?>
	                            <div class="danhgia-sb">
	                                Đánh giá : <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i>
	                            </div>
	                       	<?php } ?>
	                       	<div class="content-sb"> <?php the_content(); ?> </div>
	                       	<h3 class="h3-sb">THÔNG SỐ KỸ THUẬT</h3>
	                       	<table>
	                       		<tr>
	                       			<td>Chiều cao sợi cỏ</td>
	                       			<td>Khoảng cách dòng cỏ</td>
	                       			<td>Mật độ sợi cỏ</td>
	                       			<td>Số lớp đế</td>
	                       		</tr>
	                       		<tr>
	                       			<td><?php echo get_post_meta(get_the_id(),'chieu_cao',true); ?></td>
	                       			<td><?php echo get_post_meta(get_the_id(),'khoang_cach',true); ?></td>
	                       			<td><?php echo get_post_meta(get_the_id(),'mat_do',true); ?></td>
	                       			<td><?php echo get_post_meta(get_the_id(),'so_lop',true); ?></td>
	                       		</tr>
	                       	</table>
	                       	<h3 class="h3-cnt">GỌI TƯ VẤN</h3>
	                       	<h3 class="h3-cnt"><span>Hotline:</span> 0983 886 447</h3>
		                </div>
					</div>
				</div>
				<h1 class="home-title cnt-title">SẢN PHẨM TƯƠNG TỰ</h1>
				<div class="cnt-sptt"><?php echo do_shortcode( '[get_CTCNTSV]' ); ?></div>
			<?php 
				}
			}
			?>
		</div>
	</div>
</section>


<?php get_footer();