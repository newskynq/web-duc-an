<?php
// Add custom Theme Functions here
// 
function isn_scripts_and_styles(){
	wp_enqueue_style( 'isn-bootstrap-style', "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css", array(),time() );
	wp_enqueue_style( 'isn-fontawesome-style', "https://use.fontawesome.com/releases/v5.4.1/css/all.css", array(),time() );
	wp_enqueue_script( 'bootstrapjs', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', array( 'jquery' ), time(), true, true );
	wp_enqueue_script( 'isn-countUp-js', '/wp-content/themes/flatsome-child/assets/js/countUp.min.js', array(), time(), true, true );
	wp_enqueue_script( 'isn-main-js', get_template_directory_uri().'-child'.'/assets/js/main.js', array(), time(), true, true );
}

add_action( 'wp_enqueue_scripts', 'isn_scripts_and_styles' );

function tao_custom_post_CNTSB()
{
 
    /*
     * Biến $label để chứa các text liên quan đến tên hiển thị của Post Type trong Admin
     */
    $label = array(
        'name' => 'các loại cỏ s', //Tên post type dạng số nhiều
        'singular_name' => 'các loại cỏ' //Tên post type dạng số ít
    );
 
    /*
     * Biến $args là những tham số quan trọng trong Post Type
     */
    $args = array(
        'labels' => $label, //Gọi các label trong biến $label ở trên
        'description' => 'Đăng post trong cỏ nhân tạo sân bóng', //Mô tả của post type
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'author',
            'thumbnail',
            'comments',
            'trackbacks',
            'revisions',
            'custom-fields'
            
        ), //Các tính năng được hỗ trợ trong post type
        'taxonomies' => array( 'category', 'post_tag' ), //Các taxonomy được phép sử dụng để phân loại nội dung
        'hierarchical' => false, //Cho phép phân cấp, nếu là false thì post type này giống như Post, true thì giống như Page
        'public' => true, //Kích hoạt post type
        'show_ui' => true, //Hiển thị khung quản trị như Post/Page
        'show_in_menu' => true, //Hiển thị trên Admin Menu (tay trái)
        'show_in_nav_menus' => true, //Hiển thị trong Appearance -> Menus
        'show_in_admin_bar' => true, //Hiển thị trên thanh Admin bar màu đen.
        'menu_position' => 5, //Thứ tự vị trí hiển thị trong menu (tay trái)
        'menu_icon' => '', //Đường dẫn tới icon sẽ hiển thị
        'can_export' => true, //Có thể export nội dung bằng Tools -> Export
        'has_archive' => true, //Cho phép lưu trữ (month, date, year)
        'exclude_from_search' => false, //Loại bỏ khỏi kết quả tìm kiếm
        'publicly_queryable' => true, //Hiển thị các tham số trong query, phải đặt true
        'capability_type' => 'post' //
    );
 
    register_post_type('CNT', $args); //Tạo post type với slug tên là sanpham và các tham số trong biến $args ở trên
 
}
/* Kích hoạt hàm tạo custom post type */
add_action('init', 'tao_custom_post_CNTSB');

add_filter('pre_get_posts','lay_custom_post_type');
function lay_custom_post_type($query) {
  if (is_home() && $query->is_main_query ())
    $query->set ('post_type', array ('post','CNT'));
    return $query;
}

if( !function_exists( 'isn_bootstrap_pagination' ) ) {
    /**
     * [isn_bootstrap_pagination]
     * @author thanhnam <namtt@acro.vn> 
     */
    function isn_bootstrap_pagination( $query=null ) {
     
        global $wp_query;
        $query = $query ? $query : $wp_query;
        $big = 999999999;   
     
        $paginate = paginate_links( array(
                'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                'type' => 'array',
                'total' => $query->max_num_pages,
                'format' => '?paged=%#%',
                'current' => max( 1, get_query_var('paged') ),
                'prev_text' => '&laquo;',
                'next_text' => '&raquo;',
            )
        );
     
        if ($query->max_num_pages > 1) :
        ?>
            <ul class="pagination">
        <?php
                foreach ( $paginate as $page ) {
                    echo '<li>' . $page . '</li>';
                }
        ?>
            </ul>
        <?php
        endif;
    }
}

function get_CNTSB1() {
    ob_start();
    //$paged1 = ( get_query_var('page') ) ? get_query_var('page') : 1;

    $CNTSBPostsArgs = array(
        'post_type' => 'CNT',
        'posts_per_page' => 6,
        'paged' => $paged1,
         'orderby' => 'ID',
        'category_name' => 'Cỏ nhân tạo sân bóng'

    );
    $CNTSBPosts = new WP_Query( $CNTSBPostsArgs );
	?>	<div class="home-sb">    <?php 
	    if( $CNTSBPosts->have_posts() ) { 
	        while( $CNTSBPosts->have_posts() ) { 
	            $CNTSBPosts->the_post();
	            ?>
                    <div class="post-sb">
                        <a href="<?php the_permalink(); ?>">
                            <div class="image-sb"><?php the_post_thumbnail(); ?>  </div> 
                            <h3 class="h3-sb"><?php echo the_title(); ?> </h3>
                            <div class="content-sb"> <?php the_excerpt(); ?> </div>
                        </a>
                    </div> 
	            <?php
	        }
	    }
	?> </div> <?php
    ?>
        <div class="text-center">
            <?php isn_bootstrap_pagination($CNTSBPosts); ?>
        </div>
    <?php
    wp_reset_postdata();
    return ob_get_clean();
}
add_shortcode( 'get_CNTSB1', 'get_CNTSB1');

function get_CNTSB2() {
    ob_start();
    //$paged1 = ( get_query_var('page') ) ? get_query_var('page') : 1;

    $CNTSBPostsArgs = array(
        'post_type' => 'CNT',
        'posts_per_page' => 6,
        'paged' => $paged1,
         'orderby' => 'ID',
        'category_name' => 'Cỏ nhân tạo sân bóng'

    );
    $CNTSBPosts = new WP_Query( $CNTSBPostsArgs );
    ?>  <div class="home-sb">    <?php 
        if( $CNTSBPosts->have_posts() ) { 
            while( $CNTSBPosts->have_posts() ) { 
                $CNTSBPosts->the_post();
                ?>
                    <div class="post-sb">
                        <a href="<?php the_permalink(); ?>">
                            <div class="image-sb"><?php the_post_thumbnail(); ?>  </div> 
                            <h3 class="h3-sb"><?php echo the_title(); ?> </h3>
                             
                            <div class="gia_sb"><span> Giá : </span> <?php echo get_post_meta(get_the_id(),'gia_',true); ?> </div>
                            <?php if ( get_post_meta(get_the_id(),'danh_gia',true)==1 ) { ?>
                                    <div class="danhgia-sb">
                                        Đánh giá : <i class="fa fa-star" aria-hidden="true"></i></i>
                                    </div>
                                <?php } ?>
                                <?php if (get_post_meta(get_the_id(),'danh_gia',true)==2) { ?>
                                    <div class="danhgia-sb">
                                        Đánh giá : <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i>
                                    </div>
                                <?php } ?>
                                <?php if (get_post_meta(get_the_id(),'danh_gia',true)==3) { ?>
                                    <div class="danhgia-sb">
                                        Đánh giá : <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i>
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    </div>
                                <?php } ?>
                                <?php if (get_post_meta(get_the_id(),'danh_gia',true)==4) { ?>
                                    <div class="danhgia-sb">
                                        Đánh giá : <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i>
                                                    <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i>
                                    </div>
                                <?php } ?>
                                <?php if (get_post_meta(get_the_id(),'danh_gia',true)==5) { ?>
                                    <div class="danhgia-sb">
                                        Đánh giá : <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i>
                                    </div>
                            <?php } ?>    
                            <div class="content-sb"> <?php the_excerpt(); ?> </div>
                        </a>
                    </div> 
                <?php
            }
        }
    ?> </div> <?php
    ?>
        <div class="text-center">
            <?php isn_bootstrap_pagination($CNTSBPosts); ?>
        </div>
    <?php
    wp_reset_postdata();
    return ob_get_clean();
}
add_shortcode( 'get_CNTSB2', 'get_CNTSB2');

function get_CNTSV1() {
    ob_start();


    $CNTSBPostsArgs = array(
        'post_type' => 'CNT',
        'posts_per_page' => 6,
        'paged' => $paged1,
        'orderby' => 'ID',
        'category_name' => 'Cỏ nhân tạo sân vườn'

    );
    $CNTSBPosts = new WP_Query( $CNTSBPostsArgs );
	?>	<div class="home-sb">    <?php 
	    if( $CNTSBPosts->have_posts() ) { 
	        while( $CNTSBPosts->have_posts() ) { 
	            $CNTSBPosts->the_post();
	            ?>
	                    <div class="post-sb">
	                        <a href="<?php the_permalink(); ?>">
	                            <div class="image-sb"><?php the_post_thumbnail(); ?>  </div> 
	                            <h3 class="h3-sb"><?php echo the_title(); ?> </h3>
	                            <div class="content-sb"> <?php the_excerpt(); ?> </div>
	                        </a>
	                    </div> 
	            <?php
	        }
	    }
	?> </div> <?php
    ?>
        <div class="text-center">
            <?php isn_bootstrap_pagination($CNTSBPosts); ?>
        </div>
    <?php
    wp_reset_postdata();
    return ob_get_clean();
}
add_shortcode( 'get_CNTSV1', 'get_CNTSV1' );

function get_CNTSV2() {
    ob_start();


    $CNTSBPostsArgs = array(
        'post_type' => 'CNT',
        'posts_per_page' => 6,
        'paged' => $paged1,
        'orderby' => 'ID',
        'category_name' => 'Cỏ nhân tạo sân vườn'

    );
    $CNTSBPosts = new WP_Query( $CNTSBPostsArgs );
    ?>  <div class="home-sb">    <?php 
        if( $CNTSBPosts->have_posts() ) { 
            while( $CNTSBPosts->have_posts() ) { 
                $CNTSBPosts->the_post();
                ?>
                        <div class="post-sb">
                            <a href="<?php the_permalink(); ?>">
                                <div class="image-sb"><?php the_post_thumbnail(); ?>  </div> 
                                <h3 class="h3-sb"><?php echo the_title(); ?> </h3>
                                <div class="gia_sb"> <span> Giá : </span> <?php echo get_post_meta(get_the_id(),'gia_',true); ?> </div>
                                <?php if ( get_post_meta(get_the_id(),'danh_gia',true)==1 ) { ?>
                                    <div class="danhgia-sb">
                                        Đánh giá : <i class="fa fa-star" aria-hidden="true"></i></i>
                                    </div>
                                <?php } ?>
                                <?php if (get_post_meta(get_the_id(),'danh_gia',true)==2) { ?>
                                    <div class="danhgia-sb">
                                        Đánh giá : <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i>
                                    </div>
                                <?php } ?>
                                <?php if (get_post_meta(get_the_id(),'danh_gia',true)==3) { ?>
                                    <div class="danhgia-sb">
                                        Đánh giá : <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i>
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    </div>
                                <?php } ?>
                                <?php if (get_post_meta(get_the_id(),'danh_gia',true)==4) { ?>
                                    <div class="danhgia-sb">
                                        Đánh giá : <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i>
                                                    <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i>
                                    </div>
                                <?php } ?>
                                <?php if (get_post_meta(get_the_id(),'danh_gia',true)==5) { ?>
                                    <div class="danhgia-sb">
                                        Đánh giá : <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i>
                                    </div>
                                <?php } ?>   
                                <div class="content-sb"> <?php the_excerpt(); ?> </div>
                            </a>
                        </div> 
                <?php
            }
        }
    ?> </div> <?php
    ?>
        <div class="text-center">
            <?php isn_bootstrap_pagination($CNTSBPosts); ?>
        </div>
    <?php
    wp_reset_postdata();
    return ob_get_clean();
}
add_shortcode( 'get_CNTSV2', 'get_CNTSV2' );

function get_CNTSG1() {
    ob_start();

    $CNTSBPostsArgs = array(
        'post_type' => 'CNT',
        'posts_per_page' => 6,
        'paged' => $paged1,
        'orderby' => 'ID',
        'category_name' => 'Cỏ nhân tạo sân golf',

    );
    $CNTSBPosts = new WP_Query( $CNTSBPostsArgs );
	?>	<div class="home-sb">    <?php 
	    if( $CNTSBPosts->have_posts() ) { 
	        while( $CNTSBPosts->have_posts() ) { 
	            $CNTSBPosts->the_post();
	            ?>
	                    <div class="post-sb">
	                        <a href="<?php the_permalink(); ?>">
	                            <div class="image-sb"><?php the_post_thumbnail(); ?>  </div> 
	                            <h3 class="h3-sb"><?php echo the_title(); ?> </h3>
	                            <div class="content-sb"> <?php the_excerpt(); ?> </div>
	                        </a>
	                    </div> 
	            <?php
	        }
	    }
	?> </div> <?php
    ?>
        <div class="text-center">
            <?php isn_bootstrap_pagination($DATBPosts); ?>
        </div>
    <?php
    wp_reset_postdata();
    return ob_get_clean();
}
add_shortcode( 'get_CNTSG1', 'get_CNTSG1' );

function get_CNTSG2() {
    ob_start();

    $CNTSBPostsArgs = array(
        'post_type' => 'CNT',
        'posts_per_page' => 6,
        'paged' => $paged1,
        'orderby' => 'ID',
        'category_name' => 'Cỏ nhân tạo sân golf',

    );
    $CNTSBPosts = new WP_Query( $CNTSBPostsArgs );
    ?>  <div class="home-sb">    <?php 
        if( $CNTSBPosts->have_posts() ) { 
            while( $CNTSBPosts->have_posts() ) { 
                $CNTSBPosts->the_post();
                ?>
                        <div class="post-sb">
                            <a href="<?php the_permalink(); ?>">
                                <div class="image-sb"><?php the_post_thumbnail(); ?>  </div> 
                                <h3 class="h3-sb"><?php echo the_title(); ?> </h3>
                                <div class="gia_sb"> <span> Giá : </span> <?php echo get_post_meta(get_the_id(),'gia_',true); ?> </div>
                                <?php if ( get_post_meta(get_the_id(),'danh_gia',true)==1 ) { ?>
                                    <div class="danhgia-sb">
                                        Đánh giá : <i class="fa fa-star" aria-hidden="true"></i></i>
                                    </div>
                                <?php } ?>
                                <?php if (get_post_meta(get_the_id(),'danh_gia',true)==2) { ?>
                                    <div class="danhgia-sb">
                                        Đánh giá : <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i>
                                    </div>
                                <?php } ?>
                                <?php if (get_post_meta(get_the_id(),'danh_gia',true)==3) { ?>
                                    <div class="danhgia-sb">
                                        Đánh giá : <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i>
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    </div>
                                <?php } ?>
                                <?php if (get_post_meta(get_the_id(),'danh_gia',true)==4) { ?>
                                    <div class="danhgia-sb">
                                        Đánh giá : <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i>
                                                    <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i>
                                    </div>
                                <?php } ?>
                                <?php if (get_post_meta(get_the_id(),'danh_gia',true)==5) { ?>
                                    <div class="danhgia-sb">
                                        Đánh giá : <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i>
                                    </div>
                                <?php } ?>                        
                                <div class="content-sb"> <?php the_excerpt(); ?> </div>
                            </a>
                        </div> 
                <?php
            }
        }
    ?> </div> <?php
    ?>
        <div class="text-center">
            <?php isn_bootstrap_pagination($DATBPosts); ?>
        </div>
    <?php
    wp_reset_postdata();
    return ob_get_clean();
}
add_shortcode( 'get_CNTSG2', 'get_CNTSG2' );

function get_ctCNTSV() {
    ob_start();


    $CNTSBPostsArgs = array(
        'post_type' => 'CNT',
        'posts_per_page' => 3,
        'paged' => $paged1,
        'orderby' => 'rand'
        

    );
    $CNTSBPosts = new WP_Query( $CNTSBPostsArgs );
    ?>  <div class="home-sb">    <?php 
        if( $CNTSBPosts->have_posts() ) { 
            while( $CNTSBPosts->have_posts() ) { 
                $CNTSBPosts->the_post();
                ?>
                        <div class="post-sb">
                            <a href="<?php the_permalink(); ?>">
                                <div class="image-sb"><?php the_post_thumbnail(); ?>  </div> 
                                <h3 class="h3-sb"><?php echo the_title(); ?> </h3>
                                <div class="content-sb"> <?php the_excerpt(); ?> </div>
                            </a>
                        </div> 
                <?php
            }
        }
    ?> </div> <?php
    ?>
        <div class="text-center">
            <?php isn_bootstrap_pagination($CNTSBPosts); ?>
        </div>
    <?php
    wp_reset_postdata();
    return ob_get_clean();
}
add_shortcode( 'get_CTCNTSV', 'get_CTCNTSV' );

function tao_custom_post_DATB()
{
 
    /*
     * Biến $label để chứa các text liên quan đến tên hiển thị của Post Type trong Admin
     */
    $label = array(
        'name' => 'Dự án tiêu biểu', //Tên post type dạng số nhiều
        'singular_name' => 'Dự án tiêu biểu' //Tên post type dạng số ít
    );
 
    /*
     * Biến $args là những tham số quan trọng trong Post Type
     */
    $args = array(
        'labels' => $label, //Gọi các label trong biến $label ở trên
        'description' => 'Đăng post đăng dự án tiêu biểu', //Mô tả của post type
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'author',
            'thumbnail',
            'comments',
            'trackbacks',
            'revisions',
            'custom-fields'
        ), //Các tính năng được hỗ trợ trong post type
        'taxonomies' => array( 'category', 'post_tag' ), //Các taxonomy được phép sử dụng để phân loại nội dung
        'hierarchical' => false, //Cho phép phân cấp, nếu là false thì post type này giống như Post, true thì giống như Page
        'public' => true, //Kích hoạt post type
        'show_ui' => true, //Hiển thị khung quản trị như Post/Page
        'show_in_menu' => true, //Hiển thị trên Admin Menu (tay trái)
        'show_in_nav_menus' => true, //Hiển thị trong Appearance -> Menus
        'show_in_admin_bar' => true, //Hiển thị trên thanh Admin bar màu đen.
        'menu_position' => 5, //Thứ tự vị trí hiển thị trong menu (tay trái)
        'menu_icon' => '', //Đường dẫn tới icon sẽ hiển thị
        'can_export' => true, //Có thể export nội dung bằng Tools -> Export
        'has_archive' => true, //Cho phép lưu trữ (month, date, year)
        'exclude_from_search' => false, //Loại bỏ khỏi kết quả tìm kiếm
        'publicly_queryable' => true, //Hiển thị các tham số trong query, phải đặt true
        'capability_type' => 'post' //
    );
 
    register_post_type('DATB', $args); //Tạo post type với slug tên là sanpham và các tham số trong biến $args ở trên
 
}
/* Kích hoạt hàm tạo custom post type */
add_action('init', 'tao_custom_post_DATB');

add_filter('pre_get_posts','lay_custom_post_type2');
function lay_custom_post_type2($query) {
  if (is_home() && $query->is_main_query ())
    $query->set ('post_type', array ('post','DATB'));
    return $query;
}

function get_DATB1() {
    ob_start();

    $paged_DATB2 = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

    $DATBPostsArgs = array(
        'post_type' => 'DATB',
        'posts_per_page' => 15,
        'paged' => $paged_DATB2,
        'orderby' => 'ID',


    );
    $DATBPosts = new WP_Query( $DATBPostsArgs );
    ?>  <div class="home-sb">    <?php 
        if( $DATBPosts->have_posts() ) { 
            while( $DATBPosts->have_posts() ) { 
                $DATBPosts->the_post();
                ?>
                        <div class="post-sb">
                            <a href="<?php the_permalink(); ?>">
                                <div class="image-sb"><?php the_post_thumbnail(); ?>  </div> 
                                <h3 class="h3-sb"><?php echo the_title(); ?> </h3>
                                <div class="content-sb"> <?php the_excerpt(); ?> </div>
                            </a>
                        </div> 
                <?php
            }
        }
    ?> </div> <?php
    ?>
        <div class="text-center">
            <?php isn_bootstrap_pagination($DATBPosts); ?>
        </div>
    <?php
    wp_reset_postdata();
    return ob_get_clean();
}
add_shortcode( 'get_DATB1', 'get_DATB1' );

function get_DATB2() {
    ob_start();

    $DATBPostsArgs = array(
        'post_type' => 'DATB',
        'posts_per_page' => 4,
        'paged' => $paged_DATB2,
        'orderby' => 'ID',


    );
    $DATBPosts = new WP_Query( $DATBPostsArgs );
    ?>  <div class="home-sb">    <?php 
        if( $DATBPosts->have_posts() ) { 
            while( $DATBPosts->have_posts() ) { 
                $DATBPosts->the_post();
                ?>
                        <div class="post-sb">
                            <a href="<?php the_permalink(); ?>">
                                <div class="image-sb"><?php the_post_thumbnail(); ?>  </div> 
                                <h3 class="h3-sb"><?php echo the_title(); ?> </h3>
                                <div class="content-sb"> <?php the_excerpt(); ?> </div>
                            </a>
                        </div> 
                <?php
            }
        }
    ?> </div> <?php
    ?>
        <div class="text-center">
            <?php isn_bootstrap_pagination($DATBPosts); ?>
        </div>
    <?php
    wp_reset_postdata();
    return ob_get_clean();
}
add_shortcode( 'get_DATB2', 'get_DATB2' );

function get_sDATB() {
    ob_start();

    $paged_DATB2 = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

    $DATBPostsArgs = array(
        'post_type' => 'DATB',
        'posts_per_page' => 3,
        'paged' => $paged_DATB2,
        'orderby' => 'rand',


    );
    $DATBPosts = new WP_Query( $DATBPostsArgs );
    ?>  <div class="home-tvsb">    <?php 
        if( $DATBPosts->have_posts() ) { 
            while( $DATBPosts->have_posts() ) { 
                $DATBPosts->the_post();
                ?>
                        <div class="post-tvsb">
                            <a href="<?php the_permalink(); ?>">
                                <div class="image-tvsb"><?php the_post_thumbnail(); ?>  </div> 
                                <h3 class="h3-tvsb"><?php echo the_title(); ?> </h3>
                                <div class="content-tvsb"> <?php the_excerpt(); ?> </div>
                            </a>
                        </div> 
                <?php
            }
        }
    ?> </div> <?php
    ?>
        <div class="text-center">
            <?php isn_bootstrap_pagination($DATBPosts); ?>
        </div>
    <?php
    wp_reset_postdata();
    return ob_get_clean();
}
add_shortcode( 'get_sDATB', 'get_sDATB' );

function tao_custom_post_TV()
{
 
    /*
     * Biến $label để chứa các text liên quan đến tên hiển thị của Post Type trong Admin
     */
    $label = array(
        'name' => 'Tư vấn', //Tên post type dạng số nhiều
        'singular_name' => 'Tư vấn' //Tên post type dạng số ít
    );
 
    /*
     * Biến $args là những tham số quan trọng trong Post Type
     */
    $args = array(
        'labels' => $label, //Gọi các label trong biến $label ở trên
        'description' => 'Đăng post đăng tư vấn', //Mô tả của post type
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'author',
            'thumbnail',
            'comments',
            'trackbacks',
            'revisions',
            'custom-fields'
        ), //Các tính năng được hỗ trợ trong post type
        'taxonomies' => array( 'category', 'post_tag' ), //Các taxonomy được phép sử dụng để phân loại nội dung
        'hierarchical' => false, //Cho phép phân cấp, nếu là false thì post type này giống như Post, true thì giống như Page
        'public' => true, //Kích hoạt post type
        'show_ui' => true, //Hiển thị khung quản trị như Post/Page
        'show_in_menu' => true, //Hiển thị trên Admin Menu (tay trái)
        'show_in_nav_menus' => true, //Hiển thị trong Appearance -> Menus
        'show_in_admin_bar' => true, //Hiển thị trên thanh Admin bar màu đen.
        'menu_position' => 5, //Thứ tự vị trí hiển thị trong menu (tay trái)
        'menu_icon' => '', //Đường dẫn tới icon sẽ hiển thị
        'can_export' => true, //Có thể export nội dung bằng Tools -> Export
        'has_archive' => true, //Cho phép lưu trữ (month, date, year)
        'exclude_from_search' => false, //Loại bỏ khỏi kết quả tìm kiếm
        'publicly_queryable' => true, //Hiển thị các tham số trong query, phải đặt true
        'capability_type' => 'product' //
    );
 
    register_post_type('TV', $args); //Tạo post type với slug tên là sanpham và các tham số trong biến $args ở trên
 
}
/* Kích hoạt hàm tạo custom post type */
add_action('init', 'tao_custom_post_TV');

add_filter('pre_get_posts','lay_custom_post_type3');
function lay_custom_post_type3($query) {
  if (is_home() && $query->is_main_query ())
    $query->set ('post_type', array ('post','TV'));
    return $query;
}

function get_tvsb() {
    ob_start();

    $paged_DATB3 = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

    $DATBPostsArgs = array(
        'post_type' => 'TV',
        'posts_per_page' => 4,
        'paged' => $paged_DATB3,
        'orderby' => 'ID',
        'category_name' => 'Tư vấn sân bóng'

    );
    $DATBPosts = new WP_Query( $DATBPostsArgs );
    ?>  <div class="home-tvsb">    <?php 
        if( $DATBPosts->have_posts() ) { 
            while( $DATBPosts->have_posts() ) { 
                $DATBPosts->the_post();
                ?>
                        <div class="post-tvsb">
                            <a href="<?php the_permalink(); ?>">
                                <div class="image-tvsb"><?php the_post_thumbnail(); ?>  </div> 
                                <h3 class="h3-tvsb"><?php echo the_title(); ?> </h3>
                                <div class="content-tvsb"> <?php the_excerpt(); ?> </div>
                            </a>
                        </div> 
                <?php
            }
        }
    ?> </div> <?php
    ?>
        <div class="text-center">
            <?php isn_bootstrap_pagination($DATBPosts); ?>
        </div>
    <?php
    wp_reset_postdata();
    return ob_get_clean();
}
add_shortcode( 'get_tvsb', 'get_tvsb' );

function get_tvsv() {
    ob_start();



    $DATBPostsArgs = array(
        'post_type' => 'TV',
        'posts_per_page' => 4,
        'paged' => $paged_DATB3,
        'orderby' => 'ID',
        'category_name' => 'Tư vấn sân vườn'

    );
    $DATBPosts = new WP_Query( $DATBPostsArgs );
    ?>  <div class="home-tvsb">    <?php 
        if( $DATBPosts->have_posts() ) { 
            while( $DATBPosts->have_posts() ) { 
                $DATBPosts->the_post();
                ?>
                        <div class="post-tvsb">
                            <a href="<?php the_permalink(); ?>">
                                <div class="image-tvsb"><?php the_post_thumbnail(); ?>  </div> 
                                <h3 class="h3-tvsb"><?php echo the_title(); ?> </h3>
                                <div class="content-tvsb"> <?php the_excerpt(); ?> </div>
                            </a>
                        </div> 
                <?php
            }
        }
    ?> </div> <?php
    ?>
        <div class="text-center">
            <?php isn_bootstrap_pagination($DATBPosts); ?>
        </div>
    <?php
    wp_reset_postdata();
    return ob_get_clean();
}
add_shortcode( 'get_tvsv', 'get_tvsv' );

function get_tvsg() {
    ob_start();



    $DATBPostsArgs = array(
        'post_type' => 'TV',
        'posts_per_page' => 4,
        'paged' => $paged_DATB3,

        'orderby' => 'ID',
        'category_name' => 'Tư vấn sân golf'

    );
    $DATBPosts = new WP_Query( $DATBPostsArgs );
    ?>  <div class="home-tvsb">    <?php 
        if( $DATBPosts->have_posts() ) { 
            while( $DATBPosts->have_posts() ) { 
                $DATBPosts->the_post();
                ?>
                        <div class="post-tvsb">
                            <a href="<?php the_permalink(); ?>">
                                <div class="image-tvsb"><?php the_post_thumbnail(); ?>  </div> 
                                <h3 class="h3-tvsb"><?php echo the_title(); ?> </h3>
                                <div class="content-tvsb"> <?php the_excerpt(); ?> </div>
                            </a>
                        </div> 
                <?php
            }
        }
    ?> </div> <?php
    ?>
        <div class="text-center">
            <?php isn_bootstrap_pagination($DATBPosts); ?>
        </div>
    <?php
    wp_reset_postdata();
    return ob_get_clean();
}
add_shortcode( 'get_tvsg', 'get_tvsg' );

function get_tvkt() {
    ob_start();



    $DATBPostsArgs = array(
        'post_type' => 'TV',
        'posts_per_page' => 12,
        'paged' => $paged_DATB3,

        'orderby' => 'ID',
        'category_name' => 'Tư vấn kỹ thuật sân cỏ nhân tạo'

    );
    $DATBPosts = new WP_Query( $DATBPostsArgs );
    ?>  <div class="home-tvsb">    <?php 
        if( $DATBPosts->have_posts() ) { 
            while( $DATBPosts->have_posts() ) { 
                $DATBPosts->the_post();
                ?>
                        <div class="post-tvsb">
                            <a href="<?php the_permalink(); ?>">
                                <div class="image-tvsb"><?php the_post_thumbnail(); ?>  </div> 
                                <h3 class="h3-tvsb"><?php echo the_title(); ?> </h3>
                                <div class="content-tvsb"> <?php the_excerpt(); ?> </div>
                            </a>
                        </div> 
                <?php
            }
        }
    ?> </div> <?php
    ?>
        <div class="text-center">
            <?php isn_bootstrap_pagination($DATBPosts); ?>
        </div>
    <?php
    wp_reset_postdata();
    return ob_get_clean();
}
add_shortcode( 'get_tvkt', 'get_tvkt' );

function get_tv() {
    ob_start();

    $paged_DATB4 = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

    $DATBPostsArgs = array(
        'post_type' => 'TV',
        'posts_per_page' => 7,
        'paged' => $paged_DATB4,

        'orderby' => 'ID'

    );
    $DATBPosts = new WP_Query( $DATBPostsArgs );
    ?>  <div class="home-tvsb">    <?php 
        if( $DATBPosts->have_posts() ) { 
            while( $DATBPosts->have_posts() ) { 
                $DATBPosts->the_post();
                ?>
                        <div class="post-tv">
                            <a href="<?php the_permalink(); ?>">
                                <div class="image-tv"><?php the_post_thumbnail(); ?>  </div> 
                                <h3 class="h3-tv"><?php echo the_title(); ?> </h3>
                                <div class="content-tv"> <?php the_excerpt(); ?> </div>
                            </a>
                        </div> 
                <?php
            }
        }
    ?> </div> <?php
    ?>
        <div class="text-center">
            <?php isn_bootstrap_pagination($DATBPosts); ?>
        </div>
    <?php
    wp_reset_postdata();
    return ob_get_clean();
}
add_shortcode( 'get_tv', 'get_tv' );

function get_tvk() {
    ob_start();

    $paged_DATB4 = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

    $DATBPostsArgs = array(
        'post_type' => 'TV',
        'posts_per_page' => 3,
        'paged' => $paged_DATB4,
        'orderby' => 'ID'

    );
    $DATBPosts = new WP_Query( $DATBPostsArgs );
    ?>  <div class="home-tvK">    <?php 
        if( $DATBPosts->have_posts() ) { 
            while( $DATBPosts->have_posts() ) { 
                $DATBPosts->the_post();
                ?>
                        <div class="post-tvK">
                            <a href="<?php the_permalink(); ?>">
                                
                                <div class="text-tvk"><?php echo the_title(); ?> </div>
                                
                            </a>
                        </div> 
                <?php
            }
        }
    ?> </div> <?php
    ?>
        <div class="text-center">
            <?php isn_bootstrap_pagination($DATBPosts); ?>
        </div>
    <?php
    wp_reset_postdata();
    return ob_get_clean();
}
add_shortcode( 'get_tvk', 'get_tvk' );

function get_stv() {
    ob_start();

    $paged_DATB4 = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

    $DATBPostsArgs = array(
        'post_type' => 'TV',
        'posts_per_page' => 8,
        'paged' => $paged_DATB4,
        'orderby' => 'ID',
        'category_name' => 'Tư vấn kỹ thuật sân cỏ nhân tạo'
    );
    $DATBPosts = new WP_Query( $DATBPostsArgs );
    ?>  <div class="home-tvsb">    <?php 
        if( $DATBPosts->have_posts() ) { 
            while( $DATBPosts->have_posts() ) { 
                $DATBPosts->the_post();
                ?>
                        <div class="post-tvsb">
                            <a href="<?php the_permalink(); ?>">
                                <div class="image-tvsb"><?php the_post_thumbnail(); ?>  </div> 
                                <h3 class="h3-tvsb"><?php echo the_title(); ?> </h3>
                                <div class="content-tvsb"> <?php the_excerpt(); ?> </div>
                            </a>
                        </div> 
                <?php
            }
        }
    ?> </div> <?php
    ?>
        <div class="text-center">
            <?php isn_bootstrap_pagination($DATBPosts); ?>
        </div>
    <?php
    wp_reset_postdata();
    return ob_get_clean();
}
add_shortcode( 'get_stv', 'get_stv' );

function get_cntv() {
    ob_start();

    $paged_DATB4 = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

    $DATBPostsArgs = array(
        'post_type' => 'TV',
        'posts_per_page' => 4,
        'paged' => $paged_DATB4,
        'orderby' => 'ID',
        'category_name' => 'Cẩm nang tư vấn'
    );
    $DATBPosts = new WP_Query( $DATBPostsArgs );
    ?>  <div class="home-tvsb home-cntv">    <?php 
        if( $DATBPosts->have_posts() ) { 
            while( $DATBPosts->have_posts() ) { 
                $DATBPosts->the_post();
                ?>
                        <div class=" post-cntv">
                            <a href="<?php the_permalink(); ?>">
                                <div class="image-tv"><?php the_post_thumbnail(); ?>  </div> 
                                <h3 class="h3-tv"><?php echo the_title(); ?> </h3>
                                <div class="content-tv"> <?php the_excerpt(); ?> </div>
                            </a>
                        </div> 
                <?php
            }
        }
    ?> </div> <?php
    ?>
        <div class="text-center">
            <?php isn_bootstrap_pagination($DATBPosts); ?>
        </div>
    <?php
    wp_reset_postdata();
    return ob_get_clean();
}
add_shortcode( 'get_cntv', 'get_cntv' );

function get_ctbqt() {
    ob_start();

    $DATBPostsArgs = array(
        'post_type' => 'TV',
        'posts_per_page' => 4,
        'paged' => $paged_DATB2,
        'orderby' => 'ID',
        'category_name' => 'Có thể bạn quan tâm'

    );
    $DATBPosts = new WP_Query( $DATBPostsArgs );
    ?>  <div class="home-sb">    <?php 
        if( $DATBPosts->have_posts() ) { 
            while( $DATBPosts->have_posts() ) { 
                $DATBPosts->the_post();
                ?>
                        <div class="post-sb">
                            <a href="<?php the_permalink(); ?>">
                                <div class="image-sb"><?php the_post_thumbnail(); ?>  </div> 
                                <h3 class="h3-sb"><?php echo the_title(); ?> </h3>
                                <div class="content-sb"> <?php the_excerpt(); ?> </div>
                            </a>
                        </div> 
                <?php
            }
        }
    ?> </div> <?php
    ?>
        <div class="text-center">
            <?php isn_bootstrap_pagination($DATBPosts); ?>
        </div>
    <?php
    wp_reset_postdata();
    return ob_get_clean();
}
add_shortcode( 'get_ctbqt', 'get_ctbqt' );



function tao_custom_post_PK()
{
 
    /*
     * Biến $label để chứa các text liên quan đến tên hiển thị của Post Type trong Admin
     */
    $label = array(
        'name' => 'Phụ kiện', //Tên post type dạng số nhiều
        'singular_name' => 'Phụ kiện' //Tên post type dạng số ít
    );
 
    /*
     * Biến $args là những tham số quan trọng trong Post Type
     */
    $args = array(
        'labels' => $label, //Gọi các label trong biến $label ở trên
        'description' => 'Đăng post đăng phụ kiện', //Mô tả của post type
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'author',
            'thumbnail',
            'comments',
            'trackbacks',
            'revisions',
            'custom-fields'
        ), //Các tính năng được hỗ trợ trong post type
        'taxonomies' => array( 'category', 'post_tag' ), //Các taxonomy được phép sử dụng để phân loại nội dung
        'hierarchical' => false, //Cho phép phân cấp, nếu là false thì post type này giống như Post, true thì giống như Page
        'public' => true, //Kích hoạt post type
        'show_ui' => true, //Hiển thị khung quản trị như Post/Page
        'show_in_menu' => true, //Hiển thị trên Admin Menu (tay trái)
        'show_in_nav_menus' => true, //Hiển thị trong Appearance -> Menus
        'show_in_admin_bar' => true, //Hiển thị trên thanh Admin bar màu đen.
        'menu_position' => 5, //Thứ tự vị trí hiển thị trong menu (tay trái)
        'menu_icon' => '', //Đường dẫn tới icon sẽ hiển thị
        'can_export' => true, //Có thể export nội dung bằng Tools -> Export
        'has_archive' => true, //Cho phép lưu trữ (month, date, year)
        'exclude_from_search' => false, //Loại bỏ khỏi kết quả tìm kiếm
        'publicly_queryable' => true, //Hiển thị các tham số trong query, phải đặt true
        'capability_type' => 'post' //
    );
 
    register_post_type('PK', $args); //Tạo post type với slug tên là sanpham và các tham số trong biến $args ở trên
 
}
/* Kích hoạt hàm tạo custom post type */
add_action('init', 'tao_custom_post_PK');

add_filter('pre_get_posts','lay_custom_post_typePK');
function lay_custom_post_typePK($query) {
  if (is_home() && $query->is_main_query ())
    $query->set ('post_type', array ('post','PK'));
    return $query;
}

function get_PKSG() {
    ob_start();

    $DATBPostsArgs = array(
        'post_type' => 'PK',
        'posts_per_page' => 4,
        'paged' => $paged_DATB2,
        'order' => 'ASC',
        'orderby' => 'ID',


    );
    $DATBPosts = new WP_Query( $DATBPostsArgs );
    ?>  <div class="home-sb">    <?php 
        if( $DATBPosts->have_posts() ) { 
            while( $DATBPosts->have_posts() ) { 
                $DATBPosts->the_post();
                ?>
                        <div class="post-sb">
                            <a href="<?php the_permalink(); ?>">
                                <div class="image-sb"><?php the_post_thumbnail(); ?>  </div> 
                                <h3 class="h3-sb"><?php echo the_title(); ?> </h3>
                                <div class="content-sb"> <?php the_excerpt(); ?> </div>
                            </a>
                        </div> 
                <?php
            }
        }
    ?> </div> <?php
    ?>
        <div class="text-center">
            <?php isn_bootstrap_pagination($DATBPosts); ?>
        </div>
    <?php
    wp_reset_postdata();
    return ob_get_clean();
}
add_shortcode( 'get_PKSG', 'get_PKSG' );


// tạo custom search 

function template_chooser($template)   
{    
  global $wp_query;   
  $post_type = get_query_var('post_type');   
  if( $wp_query->is_search && $post_type == 'CNT' && $post_type == 'DATB' && $post_type == 'TV')   
  {
    return locate_template('archive-search.php');  //  redirect to archive-search.php
  }   
  return $template;   
}
add_filter('template_include', 'template_chooser'); 




function get_search()   {
    ob_start();
    ?>
        <div class="job-search">
            <form method="GET" action="">
                <input type="text" name="keyword" class="input-search" placeholder="Keyword" value="<?php if(!empty($_GET['keyword'])) echo $_GET['keyword']; ?>">
                <button><i class="fas fa-search"></i>Search</button>
            </form>
        </div>
    <?php
    $category = '';
    if($terms) {
        foreach ($terms as $term) {
            $category = $term->name;
            break;
        }
    }
    if(!empty($_GET['category'])) {
        $category = $_GET['category'];
    }
    $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

    $search_keyword = '';
    if(!empty($_GET['keyword'])) {
        $search_keyword = $_GET['keyword'];
    }
    $jobPostsArgs = array(
        'post_type' => 'CNT',
        'posts_per_page' => 15,
        'paged' => $paged,
        's'     => $search_keyword,
        'meta_query' => array(
            'relation' => 'AND',
            array('key' => 'location','compare' => 'LIKE','value' => $search_location),
        )
    );

$jobPostsArgs = array(
        'post_type' => 'DATB',
        'posts_per_page' => 15,
        'paged' => $paged,
        's'     => $search_keyword,
        'meta_query' => array(
            'relation' => 'AND',
            array('key' => 'location','compare' => 'LIKE','value' => $search_location),
        )
    );

$jobPostsArgs = array(
        'post_type' => 'DATB',
        'posts_per_page' => 15,
        'paged' => $paged,
        's'     => $search_keyword,
        'meta_query' => array(
            'relation' => 'AND',
            array('key' => 'location','compare' => 'LIKE','value' => $search_location),
        )
    );

$jobPosts = new WP_Query( $jobPostsArgs );
    if( $jobPosts->have_posts() ) { 
        while( $jobPosts->have_posts() ) { 
            $jobPosts->the_post();
            $taxonomy =  get_the_terms(get_the_ID());
            foreach ($taxonomy as $key => $value) {
                if($category == $value->name || $category==''){
                    ?>
                        <div class="job-item">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="job-item-text">
                                        <h2><?php the_title(); ?></h2>
                                        <div class="job-info">
                                            <span style="width: 27%;">Position: <?php echo get_field('position'); ?></span>
                                            <span>Salary: <?php echo get_field('salary'); ?></span>
                                            <span>Location: <?php echo get_field('location'); ?></span>
                                        </div>
                                        <p><?php echo get_field('requirements'); ?></p>
                                        <a href="<?php the_permalink(); ?>" class="btn-apply">Apply now</a>
                                    </div>          
                                </div>
                                <div class="col-md-4">
                                    <div class="job-item-img">
                                        <a href="<?php the_permalink(); ?>">
                                            <?php the_post_thumbnail(); ?>
                                        </a>
                                    </div>                              
                                </div>
                            </div>              
                        </div>
                                
                    <?php
                }
            }
        }
    }
    ?>
        <div class="text-center">
            <?php isn_bootstrap_pagination($jobPosts); ?>
        </div>
    <?php
    wp_reset_postdata();
    return ob_get_clean();
}
add_shortcode( 'get_search', 'get_search' );
