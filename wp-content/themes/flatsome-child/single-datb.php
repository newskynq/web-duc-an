<?php get_header(); ?>
<section class="cnt-banner">
	
</section>
<section class="single-cnt single-datb">
	<div class="container">
		<div class="row">
			<?php 
			if( have_posts() ) {
				while (have_posts()) {
					the_post();
					$job_id = get_the_ID();
			?>	
				<div class="col-md-8 col-sm-8 col-xs-12">
					<div class="datb1">
						<h1 class="home-title datb-title">DỰ ÁN TIÊU BIỂU</h1>
						<h3 class="h3-sb"><?php echo the_title(); ?> </h3>
						<div class="content-sb"> <?php the_content(); ?> </div>
						<div class="tacgia-datb"> <?php echo get_post_meta(get_the_id(),'tac_gia',true); ?></div>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12">
					<h1 class="home-title datb-title">TƯ VẤN KỸ THUẬT SÂN CỎ NHÂN TẠO</h1>
					<?php echo do_shortcode( '[get_stv]' ); ?>
				</div>
				<div class="col-md-8 col-sm-8 col-xs-12">
					<h3 class="h3-sb h3-dak">DỰ ÁN KHÁC</h3>
					<div class="duankhac"><?php echo do_shortcode( '[get_sDATB]' ); ?></div>
				</div>
			<?php 
				}
			}
			?>
		</div>
	</div>
</section>
<?php get_footer();